package InceptialAutomation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class GoogleSearchResultOperationPOM {
	
	public void clickOnResult(WebDriver driver) {
		driver.findElement(By.xpath("(//div[@id='rso']//child::h3)[1]")).click();
	}

	public void actionInProject(WebDriver driver) {
		driver.findElement(By.xpath("//span[text()='Projects']")).click();
		
	}
}
