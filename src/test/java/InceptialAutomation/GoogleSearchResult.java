package InceptialAutomation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class GoogleSearchResult {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			WebDriver driver;
			System.setProperty("webdriver.chrome.driver", "E:\\Chrome Driver\\chromedriver.exe");
			//Headless Browser
			ChromeOptions options = new ChromeOptions();
	        options.addArguments("--headless");     
	        options.addArguments("--disable-gpu");
	        options.addArguments("--window-size=1400,800");  
	        driver = new ChromeDriver(options);
			
			GoogleSearchResultPOM googleSearchResultPOM = new GoogleSearchResultPOM();
			googleSearchResultPOM.openGoogle(driver);
			googleSearchResultPOM.googleSearch(driver);
			googleSearchResultPOM.getTitleAndVerify(driver);
			googleSearchResultPOM.closeDriver(driver);
			
			GoogleSearchResultOperationPOM googleSearchResultOperationPOM = new GoogleSearchResultOperationPOM();
			googleSearchResultOperationPOM.clickOnResult(driver);
			googleSearchResultOperationPOM.actionInProject(driver);
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
	}

}
