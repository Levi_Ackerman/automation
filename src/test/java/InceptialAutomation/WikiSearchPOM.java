package InceptialAutomation;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WikiSearchPOM {
	static WebDriver driver;

	String link = "";
	List<String> url = new ArrayList<>();

	public void driverInit() {
		System.setProperty("webdriver.chrome.driver", "E:\\Chrome Driver\\chromedriver.exe");
		driver = new ChromeDriver();
		// Headless Browser
//		ChromeOptions options = new ChromeOptions();
//        options.addArguments("--headless");     
//        options.addArguments("--disable-gpu");
//        options.addArguments("--window-size=1400,800");  
//        driver = new ChromeDriver(options);
	}

	public void openWikipedia() {
		driver.navigate().to("https://en.wikipedia.org/wiki/Main_Page");
	}

	public void pickLinks() throws InterruptedException {
		List<WebElement> links = new ArrayList<WebElement>();

		links = driver.findElements(By.xpath("//*[@id=\"mp-tfa\"]/p/a"));
		for (int i = 1; i <= links.size(); i++) {
			link = ((links.get(i - 1).getAttribute("href")));
			url.add(link);
		}
	}

	public void getLinkTitle() {
		for (int i = 0; i < url.size(); i++) {
			System.out.print("Link: " + url.get(i));
			driver.get(url.get(i));
			System.out.print("  \n Link Page Title: ");
			System.out.println(driver.getTitle());

		}

	}

//	public void seachWIKI() {
//		driver.get("https://en.wikipedia.org/wiki/Football");
//		driver.manage().window().maximize();
//	}
//	
//	public void getLinkTitle() throws InterruptedException {
////		List<ArrayList<String>> arrayList = new ArrayList<ArrayList<String>>();
////		String[][] array = new String[arrayList.size()][];
////		for (int i = 0; i < arrayList.size(); i++) {
////		    ArrayList<String> row = arrayList.get(i);
////		    array[i] = row.toArray(new String[row.size()]);
////		}
//		
//		WebDriverWait wait = new WebDriverWait(driver, 20);
//		
//		System.out.println("//// Main ////");
//		List<WebElement> li = new ArrayList<WebElement>();
//		li = driver.findElements(By.xpath("//nav[@id='p-navigation']//child::div//child::li//a"));
//		for (int i = 0; i < li.size(); i++) {
//			Thread.sleep(1000);
//			String mainLinks = li.get(i).getAttribute("href");
//			driver.navigate().to(mainLinks);
//			System.out.println(driver.getTitle());
////			driver.navigate().refresh();
//
//			wait.until(ExpectedConditions.refreshed(ExpectedConditions.stalenessOf(li.get(i))));
//			
//			driver.navigate().back();
//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		}
//		
//		System.out.println("\n");
//		
//		System.out.println("//// Contribute ////");
//		List<WebElement> li1 = new ArrayList<WebElement>();
//		li1 = driver.findElements(By.xpath("//nav[@id='p-interaction']//child::div//child::li//a"));
//		for (int j = 0; j < li1.size()-2; j++) {
//			Thread.sleep(1000);
//			String contributorLinks = li1.get(j).getAttribute("href");
//			driver.navigate().to(contributorLinks);
//			System.out.println(driver.getTitle());
////			driver.navigate().refresh();
//			
//			wait.until(ExpectedConditions.refreshed(ExpectedConditions.stalenessOf(li1.get(j))));
//			
//			driver.navigate().back();
//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		}
//		
//	}

	public void closeDriver() {
		driver.quit();
	}

}
