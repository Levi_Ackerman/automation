package InceptialAutomation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;

public class GoogleSearchResultPOM {	
		
		public static void driverInit() {
//		System.setProperty("webdriver.chrome.driver", "E:\\Chrome Driver\\chromedriver.exe");
//		//Headless Browser
//		ChromeOptions options = new ChromeOptions();
//        options.addArguments("--headless");     
//        options.addArguments("--disable-gpu");
//        options.addArguments("--window-size=1400,800");  
//        driver = new ChromeDriver(options);
		}
		
		public void openGoogle(WebDriver driver) {
			driver.get("https://www.google.com/");
			driver.manage().window().maximize();
		}
		
		public void googleSearch(WebDriver driver) {
			driver.findElement(By.xpath("//input[@name='q']")).click();
			driver.findElement(By.xpath("//input[@type='text']")).sendKeys("selenium");
			driver.findElement(By.xpath("//input[@value='Google Search']")).click();
		}
		
		public void getTitleAndVerify(WebDriver driver) {
			String actualValue = driver.findElement(By.xpath("(//div[@id='rso']//child::h3)[1]")).getText();
//			String expectedValue = "Selenium";
//			Assert.assertEquals(actualValue,expectedValue );
			System.out.println(actualValue);
		}
		
		public void closeDriver(WebDriver driver) {
			driver.quit();
		}
}
