package InceptialAutomation;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;

public class GoogleTestNg {
	
	WebDriver driver;
	  
	  @BeforeMethod
	  public void beforeMethod() {
		  System.setProperty("webdriver.chrome.driver", "D:\\chromedriver\\chromedriver.exe");
		  driver = new ChromeDriver();
		  driver.manage().window().maximize();
		  driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  driver.get("https://www.google.com/");
	  }
	  
	  @Test(priority = 1)
	  public void GoogleTitleTest() {
		  String title = driver.getTitle();
		  System.out.println(title);
	  }
	  
	  @Test(priority = 2)
	  public void GmailLinkTest() {
		  boolean b = driver.findElement(By.xpath("//a[contains(text(),'Gmail')]")).isDisplayed();
		  System.out.println(b);
	  }
	  
	  @Test(priority = 3)
	  public void extentTest() {
		  ExtentSparkReporter spark = new ExtentSparkReporter("Automation Result.html");
		  ExtentReports extent = new ExtentReports();
		  spark.config().setTheme(Theme.DARK);
		  spark.config().setDocumentTitle("MyReport");
		  spark.config().setReportName("Extent Report");
		  extent.attachReporter(spark);
		  
		  //create test node 
			ExtentTest test = extent.createTest("GoogleTitleTest").assignAuthor("Pinaki").assignCategory("Smoke");
//					.assignDevice("Chrome 92");
			  //create step test node
		  test.pass("Title test sucessful");
		  test.pass("URL loaded");
		  
		  //create test node 
			ExtentTest test1 = extent.createTest("GmailLinkTest").assignAuthor("Pinaki").assignCategory("Regression");
//					.assignDevice("Chrome 92");
			  //create step test node
		  test1.pass("Link test sucessful");
		  test1.pass("URL loaded");
//		  test1.fail("Link test fail");
		  
		  extent.flush();
		  
	  }
	  

	  @AfterMethod
	  public void afterMethod() {
		  driver.quit();
	  }
	
}
